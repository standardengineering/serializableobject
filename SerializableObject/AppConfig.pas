﻿namespace Essy.IO;

interface

uses
  System.IO,
  System.Runtime.Serialization, 
  System.Reflection;

type
  [DataContract]
  AppConfig<T> = public class(CommonConfig<T>)
    where T is AppConfig<T>, T has constructor;
  private
    class var fData: T;
    class method get_Data: T;
  public
    class property ConfigCreated: Boolean;
    class property Data: T read get_Data write fData;
  end;
  
implementation

class method AppConfig<T>.get_Data: T;
begin
  if fData = nil then
  begin
    var fi := self.get_FileInfo(Environment.SpecialFolder.CommonApplicationData);
    var fileName := FindFileToLoad(fi.Item3, fi.Item1);
    if File.Exists(fileName) then
    begin
      try
        fData := T.CreateFromFile(fileName, nil);
      except  
        try
          if File.Exists(fileName + '~') then
          begin
            fData := T.CreateFromFile(fileName + '~', nil);
          end;
        except
          if(fi.Item4) then
          begin
            if assigned(self.ErrorOccuredDefaultConfigReturned) then
              self.ErrorOccuredDefaultConfigReturned(nil, new DefaultReturnedEventArgs(FileName := fileName));
            File.Copy(fileName, fileName + ".corrupt", true);
            fData := new T;
          end
          else 
          begin
            raise;
          end;
        end;
      end;
    end
    else
    begin
      ConfigCreated := true;
      fData := new T;   
    end;    
    fData.AutoCreateBackup := fi.Item2;
    fData.FileType := fi.Item3;
    fData.FileName := fi.Item1 + get_FileExtension(fData.FileType);
  end;
  result := fData;
end;

end.
