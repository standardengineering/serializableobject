﻿namespace Essy.IO;

interface

uses
  System.IO,
  System.Runtime.Serialization, 
  System.Reflection;

type
  [DataContract]
  UserConfig<T> = public class(CommonConfig<T>)
    where T is UserConfig<T>, T has constructor;
  private
    class var fData: T;
    class method get_Data: T;
  public
    class property Data: T read get_Data write fData;
  end;

implementation

class method UserConfig<T>.get_Data: T;
begin
  if fData = nil then
  begin
    var fi := self.get_FileInfo(Environment.SpecialFolder.LocalApplicationData);
    var fileName := FindFileToLoad(fi.Item3, fi.Item1);
    if File.Exists(fileName) then
    begin
      try
        fData := T.CreateFromFile(fileName, nil);
      except    
        if File.Exists(fileName + '~') then
        begin
          try
            fData := T.CreateFromFile(fileName + '~', nil);
          except
            if(fi.Item4) then
            begin
              if assigned(self.ErrorOccuredDefaultConfigReturned) then
                ErrorOccuredDefaultConfigReturned(nil, new DefaultReturnedEventArgs(FileName := fileName));
              File.Copy(fileName, fileName + ".corrupt", true);
              fData := new T;
            end
            else 
            begin
              raise;
            end;
          end;
        end
        else 
        begin
          if(fi.Item4) then
          begin
            fData := new T;
          end
          else 
          begin
            raise;
          end;
          end;
        end;
      end
      else
      begin
        fData := new T;
      end;
    fData.AutoCreateBackup := fi.Item2;
    fData.FileType := fi.Item3;
    fData.FileName := fi.Item1 + get_FileExtension(fData.FileType);
    end;
  result := fData;
  end;

end.
