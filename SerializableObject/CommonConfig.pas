﻿namespace Essy.IO;

interface

uses
  System.IO,
  System.Runtime.Serialization, 
  System.Reflection;

type
  [DataContract]
  CommonConfig<T> = public class(SerializableObject<T>)
  protected
    class method get_FileInfo(aSpecialFolder: Environment.SpecialFolder): tuple of (String, Boolean, SerializableObjectType,Boolean);
    class method get_AssemblyCompany: String;
    class method get_ProductName: String;
    class method get_FileExtension(aFileType: SerializableObjectType): String;
    class method FindFileToLoad(aFileType: SerializableObjectType; aFileName: String): String;
  public
    method Save();
    //This property cannot use the 'event' keyword. Oxygene will throw a FieldAccessException when this event is accessed otherwise. Keeping it as a var works for now.
    class var ErrorOccuredDefaultConfigReturned: EventHandler<DefaultReturnedEventArgs>;
    property AutoCreateBackup: Boolean;
    property FileType: SerializableObjectType;
  end;

  [AttributeUsage(AttributeTargets.Class)]
  ConfigFileNameAttribute = public class(Attribute)
  private
  public
    property FileName: String;
    property ProductName: String;
    property AutoCreateBackup: Boolean;
    property FileType: SerializableObjectType;
    property ReturnDefaultWhenExceptionHappens: Boolean;
    constructor(aFileName, aProductName: String; aFileType: SerializableObjectType; aAutoCreateBackup: Boolean; aReturnDefaultWhenExceptionHappens: Boolean := false);
  end;

  DefaultReturnedEventArgs = public class(EventArgs)
  public
    property FileName: String;
  end;

implementation

class method CommonConfig<T>.get_AssemblyCompany: String;
begin
  var ea := &Assembly.GetEntryAssembly();
  if not assigned(ea) then exit 'NullCompany';
  var attributes: array of Object := ea.GetCustomAttributes(typeOf(AssemblyCompanyAttribute), false);
  assert(attributes.Length <> 0);
  result := (attributes[0] as AssemblyCompanyAttribute).Company;
  assert(result.Length <> 0);
end;

class method CommonConfig<T>.get_ProductName: String;
begin
  var attributes: array of Object := &Assembly.GetEntryAssembly.GetCustomAttributes(typeOf(AssemblyProductAttribute), false);
  assert(attributes.Length <> 0);
  result := (attributes[0] as AssemblyProductAttribute).Product;  
  assert(result.Length <> 0);
end;

class method CommonConfig<T>.get_FileInfo(aSpecialFolder: Environment.SpecialFolder): tuple of (String, Boolean, SerializableObjectType, Boolean);
begin
  var memberInfoOfT := typeOf(T) as MemberInfo;
  var attributesOfT := memberInfoOfT.GetCustomAttributes(typeOf(ConfigFileNameAttribute), false);
  if attributesOfT.Length > 0 then
  begin
    var cfna := attributesOfT[0] as ConfigFileNameAttribute;
    var rootPathName := Environment.GetFolderPath(aSpecialFolder) + Path.DirectorySeparatorChar + self.get_AssemblyCompany; 
    var pathName := rootPathName + Path.DirectorySeparatorChar + iif(String.IsNullOrEmpty(cfna.ProductName), get_ProductName(), cfna.ProductName);
    if not Directory.Exists(pathName) then
    begin
      Directory.CreateDirectory(pathName);
    end;
    var fn := pathName + Path.DirectorySeparatorChar + cfna.FileName; 
    result := [fn, cfna.AutoCreateBackup, cfna.FileType, cfna.ReturnDefaultWhenExceptionHappens];
  end
  else
  begin
    raise new Exception('ConfigFileNameAttribute is not defined.');
  end;
end;

method CommonConfig<T>.Save();
begin
  if AutoCreateBackup then 
  begin
    var fn := self.FileName;
    self.SaveToFile(fn + '~', FileType, nil);
    self.SaveToFile(fn, FileType, nil);
  end
  else 
  begin
    self.SaveToFile(self.FileName, FileType, nil);
  end;
end;

class method CommonConfig<T>.get_FileExtension(aFileType: SerializableObjectType): String;
begin
  result := case aFileType of
    SerializableObjectType.Xml: '.xml';
    SerializableObjectType.JSON: '.json';
    SerializableObjectType.CompressedXML: '.xmlx';
    SerializableObjectType.CompressedJSON: '.jsonx';
  end;
end;

class method CommonConfig<T>.FindFileToLoad(aFileType: SerializableObjectType; aFileName: String): String;
begin
  var defaultFN := aFileName + get_FileExtension(aFileType);
  if File.Exists(defaultFN) then exit defaultFN;
  defaultFN := aFileName + get_FileExtension(SerializableObjectType.Xml);
  if File.Exists(defaultFN) then exit defaultFN;
  defaultFN := aFileName + get_FileExtension(SerializableObjectType.CompressedXML);
  if File.Exists(defaultFN) then exit defaultFN;
  defaultFN := aFileName + get_FileExtension(SerializableObjectType.JSON);
  if File.Exists(defaultFN) then exit defaultFN;
  defaultFN := aFileName + get_FileExtension(SerializableObjectType.CompressedJSON);
end;

constructor ConfigFileNameAttribute(aFileName, aProductName: String; aFileType: SerializableObjectType; aAutoCreateBackup: Boolean; aReturnDefaultWhenExceptionHappens: Boolean := false);
begin
  self.FileName := aFileName;
  self.ProductName := aProductName;
  self.AutoCreateBackup := aAutoCreateBackup;
  self.FileType := aFileType;
  self.ReturnDefaultWhenExceptionHappens := aReturnDefaultWhenExceptionHappens;
end;

end.
